package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if (nodes.contains(node)) {
            nodes.add(node);
        }
        else {
            nodes.add(node);
            Set <V> nodeEdges = new HashSet<>();
            nodeToNode.put(node, nodeEdges);
        }
    }

    @Override
    public void removeNode(V node) {
        nodes.remove(node);
        nodeToNode.remove(node);
        for (V nodeInNodes : nodes) {
            Set <V> edges = nodeToNode.get(nodeInNodes);
            if (edges.contains(node)) {
                edges.remove(node);
                nodeToNode.put(nodeInNodes, edges);
            }
        }
    }

    @Override
    public void addEdge(V u, V v) {
        if (nodes.contains(u) && nodes.contains(v)) {
            Set <V> uEdges = nodeToNode.get(u);
            Set <V> vEdges = nodeToNode.get(v);
            if (uEdges != null && vEdges != null) {
                uEdges.add(v);
                vEdges.add(u);
            }
            else {
                uEdges = new HashSet<>();
                vEdges = new HashSet<>();
            }
            nodeToNode.put(u, uEdges);
            nodeToNode.put(v, vEdges);
        }
        else {
            throw new IllegalArgumentException("Node(s) not in graph");
        }
    }

    @Override
    public void removeEdge(V u, V v) {
        Set <V> uEdges = nodeToNode.get(u);
        Set <V> vEdges = nodeToNode.get(v);
        uEdges.remove(v);
        vEdges.remove(u);
        nodeToNode.put(u, uEdges);
        nodeToNode.put(v, vEdges);
    }

    @Override
    public boolean hasNode(V node) {
        if (nodes.contains(node)) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}