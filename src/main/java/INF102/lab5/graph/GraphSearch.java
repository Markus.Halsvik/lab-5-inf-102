package INF102.lab5.graph;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    /* 
     * Here connectedNodes will be filled with all nodes connected to the node u, nodeIterator iterates over all these nodes once,
     * and adds each node to connectedNodes if it's not already part of the set. The nodes that are directly connected to the nodes in
     * nodeIterator get added to tempNodes only if they aren't already part of connectedNodes, and nodeIterator gets assigned the nodes
     * in tempNodes at the end of its iteration. If it is then empty it means all nodes with some connection to u has been found, and the
     * while loop breaks. It then checks if v is in connectedNodes, if it is it returns true and otherwise returns false.
    */
    @Override
    public boolean connected(V u, V v) {
        Set <V> nodes = graph.getNodes();
        if (nodes.contains(u) && nodes.contains(v)) {
            Set <V> connectedNodes = new HashSet<>();
            connectedNodes.add(u);
            Set <V> nodeIterator = new HashSet<>(graph.getNeighbourhood(u));
            while (!nodeIterator.isEmpty()) {
                Set <V> tempNodes = new HashSet<>();
                for (V node : nodeIterator) {
                    if (!connectedNodes.contains(node)) {
                        connectedNodes.add(node);
                        Set <V> edges = graph.getNeighbourhood(node);
                        for (V edge : edges) {
                            if (!connectedNodes.contains(edge)) {
                                tempNodes.add(edge);
                            }
                        }
                    }
                }
                nodeIterator = tempNodes;
            }
            if (connectedNodes.contains(v)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}